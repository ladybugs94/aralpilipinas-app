package com.appgradingtechnology.labyalo.config;

import com.appgradingtechnology.labyalo.vendor.android.java.security.SecurityLayer;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Url extends SecurityLayer {
    public static final String PRODUCTION_URL = decrypt("http://internal.aral-pilipinas.highlysucceed.com/");
    public static final String DEBUG_URL = decrypt("http://internal.aral-pilipinas.highlysucceed.com/");

    public static final String APP = App.production ? PRODUCTION_URL : DEBUG_URL;

    public static final String getLoginStudent(){return "/api/student/login.json";}
    public static final String getLoginTeacher(){return "/api/teacher/login.json";}
    public static final String getCheckLoginStudent(){return "/api/student/check-login.json";}
    public static final String getRegisterStudent(){return "/api/student/register.json";}
    public static final String getLogoutStudent() {return "/api/student/logout.json";}
    public static final String getListSchool(){return "/api/school.json";}
}
