package com.appgradingtechnology.labyalo.config;

/**
 * Created by Labyalo on 8/7/2017.
 */

public class Keys {
    public static final String DEVICE_ID = "device_id";
    public static final String DEVICE_NAME = "device_name";
    public static final String DEVICE_REG_ID = "device_reg_id";
    public static final String PAGE = "page";
    public static final String PER_PAGE = "per_page";
    public static final String USER_ID = "user_id";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final String CONTACT_NUMBER = "contact_number";
    public static final String BIRTHDATE = "birthdate";
    public static final String STUDENT_ID = "student_id";
    public static final String SCHOOL_VALIDATION_CODE = "school_validation_code";
    public static final String SCHOOL_LEVEL = "school_level";
    public static final String PIN_PASSWORD = "pin_password";
    public static final String ADDRESS = "address";
    public static final String SCHOOL_ID = "school_id";
}
