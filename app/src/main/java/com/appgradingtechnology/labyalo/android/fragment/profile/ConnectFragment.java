package com.appgradingtechnology.labyalo.android.fragment.profile;

import android.util.Log;

import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.server.request.Auth;
import com.appgradingtechnology.labyalo.vendor.android.base.BaseFragment;
import com.appgradingtechnology.labyalo.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ConnectFragment extends BaseFragment {
    public static final String TAG = ConnectFragment.class.getName().toString();

    public static ConnectFragment newInstance() {
        ConnectFragment fragment = new ConnectFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_status;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }
}
