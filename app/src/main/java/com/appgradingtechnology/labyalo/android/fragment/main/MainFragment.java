package com.appgradingtechnology.labyalo.android.fragment.main;

import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.activity.MainActivity;
import com.appgradingtechnology.labyalo.android.adapter.DrawerAdapter;
import com.appgradingtechnology.labyalo.data.model.api.NavDrawerModel;
import com.appgradingtechnology.labyalo.data.model.api.UserModel;
import com.appgradingtechnology.labyalo.data.preference.UserData;
import com.appgradingtechnology.labyalo.server.request.Auth;
import com.appgradingtechnology.labyalo.vendor.android.base.BaseFragment;
import com.appgradingtechnology.labyalo.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class MainFragment extends BaseFragment {
    public static final String TAG = MainFragment.class.getName().toString();
    private MainActivity activity;
    private UserData userData;


    @BindView(R.id.nameTXT) TextView nameTXT;

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_main;
    }

    @Override
    public void onViewReady() {
//        SetDrawer();
        activity = (MainActivity) getContext();
        userData = new UserData();
        nameTXT.setText(userData.getUserModel().lastname + ", " + userData.getUserModel().firstname);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @OnClick(R.id.profileIMG)
    void profileIMG(){
        activity.startAnotherActivity("profile");
    }

    @OnClick(R.id.connectBTN)
    void connectBTN(){
        activity.startAnotherActivity("connect");
    }



}
