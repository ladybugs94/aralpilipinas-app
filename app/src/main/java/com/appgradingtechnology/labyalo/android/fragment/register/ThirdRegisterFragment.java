package com.appgradingtechnology.labyalo.android.fragment.register;

import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.activity.RegisterActivity;
import com.appgradingtechnology.labyalo.data.model.api.AccountModel;
import com.appgradingtechnology.labyalo.data.model.api.ValidationModel;
import com.appgradingtechnology.labyalo.server.request.Auth;
import com.appgradingtechnology.labyalo.vendor.android.base.BaseFragment;
import com.appgradingtechnology.labyalo.vendor.server.transformer.BaseTransformer;
import com.tiper.MaterialSpinner;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ThirdRegisterFragment extends BaseFragment implements MaterialSpinner.OnItemSelectedListener {
    public static final String TAG = ThirdRegisterFragment.class.getName().toString();
    private RegisterActivity activity ;
    @BindView(R.id.userNameTXT) TextView userNameTXT;
    @BindView(R.id.studentIdET) EditText studentIdET;
    @BindView(R.id.validationCodeET) EditText validationCodeET;

    @BindView(R.id.levelSpinner)  MaterialSpinner levelSpinner;
    @BindView(R.id.schoolNameSpinner)  MaterialSpinner schoolNameSpinner;
    String level, schoolName;

    private ValidationModel validationModel;



    public static ThirdRegisterFragment newInstance() {
        ThirdRegisterFragment fragment = new ThirdRegisterFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_third_reg;
    }

    @Override
    public void onViewReady() {
        activity = (RegisterActivity) getContext();
        userNameTXT.setText("Hello , " + activity.getAccountModel().firstname + " " + activity.getAccountModel().lastname);
        populateSpinnerLevel();
        populateSpinnerSchoolName();
        levelSpinner.setOnItemSelectedListener(this);
        schoolNameSpinner.setOnItemSelectedListener(this);
        validationModel = new ValidationModel();
    }

    private void populateSpinnerSchoolName() {
        List<String> SchoolName = new ArrayList<String>();
        String [] SchoolNames = {"UP", "UCLA", "UH", "UT"};
        for (int i = 0; i <SchoolNames.length; i++) {
            SchoolName.add(SchoolNames[i]);
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, SchoolName);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        schoolNameSpinner.setAdapter(dataAdapter);
    }

    private void populateSpinnerLevel() {
        List<String> level = new ArrayList<String>();
        String [] levels = {"Primary", "Secondary", "Tertiary"};
        for (int i = 0; i <levels.length; i++) {
            level.add(levels[i]);
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, level);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        levelSpinner.setAdapter(dataAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @OnClick(R.id.nextBTN)
    void nextBTN() {
        if (!studentIdET.getText().toString().trim().isEmpty() && !validationCodeET.getText().toString().trim().isEmpty()) {
            validationModel.firstname = activity.getAccountModel().firstname;
            validationModel.lastname = activity.getAccountModel().lastname;
            validationModel.birthdate = activity.getAccountModel().birthdate;
            validationModel.email = activity.getAccountModel().email;
            validationModel.contact_number = activity.getAccountModel().contactNumber;
            validationModel.address = activity.getAccountModel().address;
            validationModel.password = activity.getAccountModel().password;
            validationModel.password_confirmation = activity.getAccountModel().password_confirmation;
            validationModel.student_id = studentIdET.getText().toString().trim();
            validationModel.school_validation_code = validationCodeET.getText().toString().trim();
            activity.setValidationModel(validationModel);
            activity.openFourthFragment();
        } else if (studentIdET.getText().toString().trim().isEmpty()) {
                studentIdET.setError("Field must not empty!");

        }
        else if (validationCodeET.getText().toString().trim().isEmpty()) {
            validationCodeET.setError("Field must not empty!");

        }
    }


    @Override
    public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view, int i, long l) {
        switch (materialSpinner.getId()){
            case R.id.levelSpinner:
                level = levelSpinner.getSelectedItem().toString();
                break;
            case R.id.schoolNameSpinner:
                schoolName = schoolNameSpinner.getSelectedItem().toString();
                break;

        }

    }

    @Override
    public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {

    }


}
