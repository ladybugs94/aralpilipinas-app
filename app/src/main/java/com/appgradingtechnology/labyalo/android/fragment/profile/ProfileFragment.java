package com.appgradingtechnology.labyalo.android.fragment.profile;

import android.content.DialogInterface;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.activity.AnotherActivity;
import com.appgradingtechnology.labyalo.android.activity.MainActivity;
import com.appgradingtechnology.labyalo.data.preference.UserData;
import com.appgradingtechnology.labyalo.server.request.Auth;
import com.appgradingtechnology.labyalo.vendor.android.base.BaseFragment;
import com.appgradingtechnology.labyalo.vendor.android.java.ToastMessage;
import com.appgradingtechnology.labyalo.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ProfileFragment extends BaseFragment {
    public static final String TAG = ProfileFragment.class.getName().toString();
    private AnotherActivity activity;
    private UserData userData;

    @BindView(R.id.nameTXT) TextView nameTXT;

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_profile;
    }

    @Override
    public void onViewReady() {
        userData = new UserData();
        activity = (AnotherActivity) getContext();
        nameTXT.setText(userData.getUserModel().lastname + ", " + userData.getUserModel().firstname);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @OnClick(R.id.planBTN)
    void planBTN(){
      activity.startAnotherActivity("plan");
    }


    @OnClick(R.id.logoutBTN)
    void logoutBTNClicked(){

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        Auth.getDefault().logoutStudent(activity);
                     //  activity.startLandingActivity("login");
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        dialog.dismiss();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("Are you sure to logout?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();



//        mainActivity.startLandingActivity("");
    }

    @Subscribe
    public void onResponse(Auth.LogoutResponse logoutResponse){
        BaseTransformer baseTransformer = logoutResponse.getData(BaseTransformer.class);
        if(baseTransformer.status){
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
            UserData.clearData();
            activity.startLandingActivity("login");
        }else{
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.FAILED);
        }
        Log.e("Message", logoutResponse.getData(BaseTransformer.class).msg);
    }



}
