package com.appgradingtechnology.labyalo.android.fragment.register;

import android.util.Log;

import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.activity.RegisterActivity;
import com.appgradingtechnology.labyalo.server.request.Auth;
import com.appgradingtechnology.labyalo.vendor.android.base.BaseFragment;
import com.appgradingtechnology.labyalo.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class StartedFragment extends BaseFragment {
    public static final String TAG = StartedFragment.class.getName().toString();
    private RegisterActivity activity;

    public static StartedFragment newInstance() {
        StartedFragment fragment = new StartedFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_started;
    }

    @Override
    public void onViewReady() {
        activity = (RegisterActivity) getContext();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @OnClick(R.id.startedBTN)
    void startedBTN(){
        activity.startLandingActivity("login");
        activity.finish();
    }
}
