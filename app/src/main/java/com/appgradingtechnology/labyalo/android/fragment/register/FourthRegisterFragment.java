package com.appgradingtechnology.labyalo.android.fragment.register;

import android.content.Context;
import android.graphics.Rect;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.activity.RegisterActivity;
import com.appgradingtechnology.labyalo.config.Keys;
import com.appgradingtechnology.labyalo.data.model.api.UserModel;
import com.appgradingtechnology.labyalo.server.request.Auth;
import com.appgradingtechnology.labyalo.vendor.android.base.BaseFragment;
import com.appgradingtechnology.labyalo.vendor.android.java.ToastMessage;
import com.appgradingtechnology.labyalo.vendor.server.request.APIRequest;
import com.appgradingtechnology.labyalo.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class FourthRegisterFragment extends BaseFragment implements TextWatcher, View.OnClickListener {
    public static final String TAG = FourthRegisterFragment.class.getName().toString();
    private RegisterActivity activity;
    private APIRequest apiRequest;
    private UserModel userModel;
    private

    boolean ispin1ETFocus = false;
    boolean ispin2ETFocus = false;
    boolean ispin3ETFocus = false;
    boolean ispin4ETFocus = false;
    @BindView(R.id.pin1ET)  EditText pin1ET;
    @BindView(R.id.pin2ET)  EditText pin2ET;
    @BindView(R.id.pin3ET)  EditText pin3ET;
    @BindView(R.id.pin4ET)  EditText pin4ET;
    @BindView(R.id.num0TXT) TextView num0TXT;
    @BindView(R.id.num1TXT) TextView num1TXT;
    @BindView(R.id.num2TXT) TextView num2TXT;
    @BindView(R.id.num3TXT) TextView num3TXT;
    @BindView(R.id.num4TXT) TextView num4TXT;
    @BindView(R.id.num5TXT) TextView num5TXT;
    @BindView(R.id.num6TXT) TextView num6TXT;
    @BindView(R.id.num7TXT) TextView num7TXT;
    @BindView(R.id.num8TXT) TextView num8TXT;
    @BindView(R.id.num9TXT) TextView num9TXT;
    @BindView(R.id.deleteIMG) ImageView deleteIMG;


    public static FourthRegisterFragment newInstance() {
        FourthRegisterFragment fragment = new FourthRegisterFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_fourth_reg;
    }

    @Override
    public void onViewReady() {
        activity = (RegisterActivity) getContext();
        pin1ET.requestFocus();
        ispin1ETFocus = true;
        setListener();
    }

    private void setListener() {
        pin1ET.addTextChangedListener(this);
        pin2ET.addTextChangedListener(this);
        pin3ET.addTextChangedListener(this);
        pin4ET.addTextChangedListener(this);

        pin1ET.setOnClickListener(this);
        pin2ET.setOnClickListener(this);
        pin3ET.setOnClickListener(this);
        pin4ET.setOnClickListener(this);

        pin1ET.setEnabled(false);
        pin2ET.setEnabled(false);
        pin3ET.setEnabled(false);
        pin4ET.setEnabled(false);

        pin1ET.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        pin2ET.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        pin3ET.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        pin4ET.setTransformationMethod(new AsteriskPasswordTransformationMethod());

        num0TXT.setOnClickListener(this);
        num1TXT.setOnClickListener(this);
        num2TXT.setOnClickListener(this);
        num3TXT.setOnClickListener(this);
        num4TXT.setOnClickListener(this);
        num5TXT.setOnClickListener(this);
        num6TXT.setOnClickListener(this);
        num7TXT.setOnClickListener(this);
        num8TXT.setOnClickListener(this);
        num9TXT.setOnClickListener(this);
        deleteIMG.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.RegisterResponse registerResponse) {
        BaseTransformer baseTransformer = registerResponse.getData(BaseTransformer.class);
        if (baseTransformer.status){
            activity.openStartedFragment();
        }
        else {
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.FAILED);

        }
    }

    @OnClick(R.id.nextBTN)
    void nextBTN()
    {
        String pin = pin1ET.getText().toString().trim() + pin2ET.getText().toString().trim() +pin3ET.getText().toString().trim() + pin4ET.getText().toString().trim();
        apiRequest = Auth.getDefault().register(activity)
                .addParameter(Keys.FIRSTNAME, activity.getValidationModel().firstname)
                .addParameter(Keys.LASTNAME, activity.getValidationModel().lastname)
                .addParameter(Keys.BIRTHDATE, activity.getValidationModel().birthdate)
                .addParameter(Keys.EMAIL, activity.getValidationModel().email)
                .addParameter(Keys.CONTACT_NUMBER, activity.getValidationModel().contact_number)
                .addParameter(Keys.ADDRESS, activity.getValidationModel().address)
                .addParameter(Keys.FIRSTNAME, activity.getValidationModel().firstname)
                .addParameter(Keys.PASSWORD, activity.getValidationModel().password)
//                .addParameter(Keys.PASSWORD_CONFIRMATION, activity.getValidationModel().password_confirmation)
                .addParameter(Keys.STUDENT_ID, activity.getValidationModel().student_id)
                .addParameter(Keys.SCHOOL_ID , 1)
                .addParameter(Keys.SCHOOL_LEVEL, activity.getValidationModel().school_level)
                .addParameter(Keys.SCHOOL_VALIDATION_CODE, activity.getValidationModel().school_validation_code)
                .addParameter(Keys.PIN_PASSWORD, pin);

        apiRequest.execute();


//        Toast.makeText(activity, " data "  + activity.getValidationModel().student_id  , Toast.LENGTH_SHORT).show();
//        activity.openStartedFragment();
    }

    public void hideSoftKeyboard(View view){
        InputMethodManager imm =(InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

        if (!pin1ET.getText().toString().isEmpty()) {
            pin2ET.requestFocus();
            ispin1ETFocus = false;
            ispin2ETFocus = true;
            ispin3ETFocus = false;
            ispin4ETFocus = false;

        }  if (!pin2ET.getText().toString().isEmpty()) {
            pin3ET.requestFocus();
            ispin1ETFocus = false;
            ispin2ETFocus = false;
            ispin3ETFocus = true;
            ispin4ETFocus = false;

        }  if (!pin3ET.getText().toString().isEmpty()) {
            pin4ET.requestFocus();
            ispin1ETFocus = false;
            ispin2ETFocus = false;
            ispin3ETFocus = false;
            ispin4ETFocus = true;
            ;

        }  if (!pin4ET.getText().toString().isEmpty()) {
            pin4ET.requestFocus();
//            code[3] = pin4ET.getText().toString().charAt(0);
//            ispin1ETFocus = false;
//            ispin2ETFocus = false;
//            ispin3ETFocus = false;
//            ispin4ETFocus = true;
        }

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.pin1ET:
                hideSoftKeyboard(pin1ET);
                break;
            case R.id.pin2ET:
                hideSoftKeyboard(pin2ET);
                break;
            case R.id.pin3ET:
                hideSoftKeyboard(pin3ET);
                break;
            case R.id.pin4ET:
                hideSoftKeyboard(pin4ET);
                break;
            case R.id.num0TXT:
                if(ispin1ETFocus) {
                    pin1ET.setText("0");
                }
                else if(ispin2ETFocus){
                    pin2ET.setText("0");
                }
                else if(ispin3ETFocus){
                    pin3ET.setText("0");
                }
                else if(ispin4ETFocus){
                    pin4ET.setText("0");
                }
                break;
            case R.id.num1TXT:
                if(ispin1ETFocus) {
                    pin1ET.setText("1");
                }
                else if(ispin2ETFocus){
                    pin2ET.setText("1");
                }
                else if(ispin3ETFocus){
                    pin3ET.setText("1");
                }
                else if(ispin4ETFocus){
                    pin4ET.setText("1");
                }
                break;
            case R.id.num2TXT:
                if(ispin1ETFocus) {
                    pin1ET.setText("2");
                }
                else if(ispin2ETFocus){
                    pin2ET.setText("2");
                }
                else if(ispin3ETFocus){
                    pin3ET.setText("2");
                }
                else if(ispin4ETFocus){
                    pin4ET.setText("2");
                }
                break;
            case R.id.num3TXT:
                if(ispin1ETFocus) {
                    pin1ET.setText("3");
                }
                else if(ispin2ETFocus){
                    pin2ET.setText("3");
                }
                else if(ispin3ETFocus){
                    pin3ET.setText("3");
                }
                else if(ispin4ETFocus){
                    pin4ET.setText("3");
                }
                break;
            case R.id.num4TXT:
                if(ispin1ETFocus) {
                    pin1ET.setText("4");
                }
                else if(ispin2ETFocus){
                    pin2ET.setText("4");
                }
                else if(ispin3ETFocus){
                    pin3ET.setText("4");
                }
                else if(ispin4ETFocus){
                    pin4ET.setText("4");
                }
                break;
            case R.id.num5TXT:
                if(ispin1ETFocus) {
                    pin1ET.setText("5");
                }
                else if(ispin2ETFocus){
                    pin2ET.setText("5");
                }
                else if(ispin3ETFocus){
                    pin3ET.setText("5");
                }
                else if(ispin4ETFocus){
                    pin4ET.setText("5");
                }
                break;
            case R.id.num6TXT:
                if(ispin1ETFocus) {
                    pin1ET.setText("6");
                }
                else if(ispin2ETFocus){
                    pin2ET.setText("6");
                }
                else if(ispin3ETFocus){
                    pin3ET.setText("6");
                }
                else if(ispin4ETFocus){
                    pin4ET.setText("6");
                }
                break;
            case R.id.num7TXT:
                if(ispin1ETFocus) {
                    pin1ET.setText("7");
                }
                else if(ispin2ETFocus){
                    pin2ET.setText("7");
                }
                else if(ispin3ETFocus){
                    pin3ET.setText("7");
                }
                else if(ispin4ETFocus){
                    pin4ET.setText("7");
                }
                break;
            case R.id.num8TXT:
                if(ispin1ETFocus) {
                    pin1ET.setText("8");
                }
                else if(ispin2ETFocus){
                    pin2ET.setText("8");
                }
                else if(ispin3ETFocus){
                    pin3ET.setText("8");
                }
                else if(ispin4ETFocus){
                    pin4ET.setText("8");
                }
                break;
            case R.id.num9TXT:
                if(ispin1ETFocus) {
                    pin1ET.setText("9");
                }
                else if(ispin2ETFocus){
                    pin2ET.setText("9");
                }
                else if(ispin3ETFocus){
                    pin3ET.setText("9");
                }
                else if(ispin4ETFocus){
                    pin4ET.setText("9");
                }
                break;
            case R.id.deleteIMG:
                if(!pin4ET.getText().toString().trim().isEmpty()){
                    pin4ET.getText().clear();
                    pin3ET.requestFocus();
                    ispin1ETFocus = false;
                    ispin2ETFocus = false;
                    ispin3ETFocus = true;
                    ispin4ETFocus = false;
                }
                else if(!pin3ET.getText().toString().trim().isEmpty()){
                    pin3ET.getText().clear();
                    pin2ET.requestFocus();
                    ispin1ETFocus = false;
                    ispin2ETFocus = true;
                    ispin3ETFocus = false;
                    ispin4ETFocus = false;
                }
                else if(!pin2ET.getText().toString().trim().isEmpty()){
                    pin2ET.getText().clear();
                    pin1ET.requestFocus();
                    ispin1ETFocus = true;
                    ispin2ETFocus = false;
                    ispin3ETFocus = false;
                    ispin4ETFocus = false;
                }
                else if(!pin1ET.getText().toString().trim().isEmpty()){
                    pin1ET.getText().clear();
                    ispin1ETFocus = true;
                    ispin2ETFocus = false;
                    ispin3ETFocus = false;
                    ispin4ETFocus = false;
                }
                break;
        }
    }

    private class AsteriskPasswordTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;
            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }
            public char charAt(int index) {
                return '\u2731'; // This is the important part
            }
            public int length() {
                return mSource.length(); // Return default
            }
            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }

        }

    }
}

