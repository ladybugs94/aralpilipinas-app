package com.appgradingtechnology.labyalo.android.fragment.landing;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.activity.LandingActivity;
import com.appgradingtechnology.labyalo.android.dialog.DefaultDialog;
import com.appgradingtechnology.labyalo.data.model.api.UserModel;
import com.appgradingtechnology.labyalo.data.preference.UserData;
import com.appgradingtechnology.labyalo.server.request.Auth;
import com.appgradingtechnology.labyalo.vendor.android.base.BaseFragment;
import com.appgradingtechnology.labyalo.vendor.android.java.ToastMessage;
import com.appgradingtechnology.labyalo.vendor.server.transformer.BaseTransformer;
import com.appgradingtechnology.labyalo.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class LoginFragment extends BaseFragment implements AdapterView.OnItemSelectedListener {
    public static final String TAG = LoginFragment.class.getName().toString();

    private LandingActivity landingActivity;
    @BindView(R.id.emailET)             EditText emailET;
    @BindView(R.id.passET)              EditText passET;
    @BindView(R.id.spinner) Spinner spinner;
    String selected;



    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_login;
    }

    @Override
    public void onViewReady() {
        landingActivity = (LandingActivity) getContext();
        spinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        try{
            SingleTransformer<UserModel> singleTransformer = loginResponse.getData(SingleTransformer.class);
            if (singleTransformer.status){
                UserData.insert(singleTransformer.data);
                UserData.insert(UserData.AUTHORIZATION, singleTransformer.token);
                Toast.makeText(getContext(), singleTransformer.msg, Toast.LENGTH_LONG).show();

                landingActivity.startMainActivity("main");
            }
            else{
                ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            }
            Log.e("Message", singleTransformer.token);

        }catch(Exception e){

        }
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
           selected = adapterView.getItemAtPosition(i).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @OnClick(R.id.loginBTN)
    void loginBTN(){
        if(selected.equals("Student")){
            if(!emailET.getText().toString().trim().isEmpty() && !passET.getText().toString().trim().isEmpty())
            Auth.getDefault().loginStudent(getContext(), emailET.getText().toString(), passET.getText().toString());
            else{
                emailET.setError("Fields must not empty!");
                passET.setError("Fields must not empty!");
            }

        }
        else{
           // Auth.getDefault().loginTeacher(getContext(), emailET.getText().toString(), passET.getText().toString());
        }
    }
}
