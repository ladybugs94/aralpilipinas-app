package com.appgradingtechnology.labyalo.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.activity.LandingActivity;

import butterknife.BindView;

public class SliderAdapter extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;
    private LandingActivity activity;

    ImageView logoIMG;
    ImageView textIMG;
    TextView   aboutTXT;

    public SliderAdapter(Context context) {
        this.context = context;
        activity = (LandingActivity) context;
    }

    public int [] sliderLogo = {R.drawable.logo, R.drawable.logo, R.drawable.logo, R.drawable.logo};
    public int [] sliderIMG = {R.drawable.img_slider1, R.drawable.img_slider2, R.drawable.img_slider3, R.drawable.img_slider4};
    public String [] sliderTXT = {"Your gateway to fast, stable, reliable and affordable data connection",
            "One-click data connection to support your digital learning needs", "Track and manage your data usage in one look and receive top alerts " ,
            "Directly buy load and request for load links to top-up your Aral Pilipinas data plan and regular mobile plans"};

    @Override
    public int getCount() {
        return sliderLogo.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view  == (LinearLayout) object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.adapter_slider, container, false);
        textIMG = view.findViewById(R.id.textIMG);
        aboutTXT = view.findViewById(R.id.introTXT);
        textIMG.setImageResource(sliderIMG[position]);
        aboutTXT.setText(sliderTXT[position]);
        container.addView(view);

        return  view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
       container.removeView((LinearLayout) object);
    }
}
