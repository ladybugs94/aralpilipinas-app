package com.appgradingtechnology.labyalo.android.fragment.register;

import android.util.Log;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.activity.RegisterActivity;
import com.appgradingtechnology.labyalo.data.model.api.AccountModel;
import com.appgradingtechnology.labyalo.server.request.Auth;
import com.appgradingtechnology.labyalo.vendor.android.base.BaseFragment;
import com.appgradingtechnology.labyalo.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class FirstRegisterFragment extends BaseFragment implements RadioGroup.OnCheckedChangeListener {
    public static final String TAG = FirstRegisterFragment.class.getName().toString();
    private RegisterActivity activity ;

    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;

    public static FirstRegisterFragment newInstance() {
        FirstRegisterFragment fragment = new FirstRegisterFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_first_reg;
    }

    @Override
    public void onViewReady() {
        activity = (RegisterActivity) getContext();
        radioGroup.setOnCheckedChangeListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }


    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        if(R.id.rdoBTNSTUD == i){
            activity.openSecondFragment();

        }
    }
}
