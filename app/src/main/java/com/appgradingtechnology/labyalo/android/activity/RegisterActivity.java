package com.appgradingtechnology.labyalo.android.activity;

import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.fragment.DefaultFragment;
import com.appgradingtechnology.labyalo.android.fragment.register.FirstRegisterFragment;
import com.appgradingtechnology.labyalo.android.fragment.register.FourthRegisterFragment;
import com.appgradingtechnology.labyalo.android.fragment.register.SecondRegisterFragment;
import com.appgradingtechnology.labyalo.android.fragment.register.SignUpFragment;
import com.appgradingtechnology.labyalo.android.fragment.register.StartedFragment;
import com.appgradingtechnology.labyalo.android.fragment.register.ThirdRegisterFragment;
import com.appgradingtechnology.labyalo.android.route.RouteActivity;
import com.appgradingtechnology.labyalo.data.model.api.AccountModel;
import com.appgradingtechnology.labyalo.data.model.api.ValidationModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class RegisterActivity extends RouteActivity {
    public static final String TAG = RegisterActivity.class.getName().toString();
    private RegisterActivity activity ;
    private AccountModel accountModels;
    private ValidationModel validationModels;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_register;
    }

    @Override
    public void onViewReady() {
    activity = (RegisterActivity) getContext();
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "first_screen":
                openFirstFragment();
                break;
            case "fourth_screen":
                openFourthFragment();
                break;
            default:
                break;
        }
    }

    public void openFirstFragment(){
        switchFragment(FirstRegisterFragment.newInstance());
    }
    public void openSecondFragment(){
        switchFragment(SecondRegisterFragment.newInstance());
    }
    public void openThirdFragment(){
        switchFragment(ThirdRegisterFragment.newInstance());
    }
    public void openFourthFragment(){
        switchFragment(FourthRegisterFragment.newInstance());
    }
    public void openStartedFragment(){
        switchFragment(StartedFragment.newInstance());
    }
    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }

    public void setAccountModel(AccountModel accountModel){
        this.accountModels = accountModel;
    }

    public AccountModel getAccountModel(){
        return accountModels;
    }

    public void setValidationModel(ValidationModel validationModel){
        this.validationModels = validationModel;
    }

    public ValidationModel getValidationModel(){
        return validationModels;
    }



}
