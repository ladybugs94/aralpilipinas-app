package com.appgradingtechnology.labyalo.android.activity;


import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.fragment.DefaultFragment;
import com.appgradingtechnology.labyalo.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class WalkthroughActivity extends RouteActivity {
    public static final String TAG = WalkthroughActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_walkthrough;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "login":
                break;
            default:
                openDefaultFragment();
                break;
        }
    }

    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }
}
