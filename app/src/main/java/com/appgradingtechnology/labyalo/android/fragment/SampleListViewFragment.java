package com.appgradingtechnology.labyalo.android.fragment;

import android.widget.ListView;

import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.adapter.DefaultListViewAdapter;
import com.appgradingtechnology.labyalo.data.model.api.SampleModel;
import com.appgradingtechnology.labyalo.vendor.android.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import icepick.State;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SampleListViewFragment extends BaseFragment {
    public static final String TAG = SampleListViewFragment.class.getName().toString();

    private DefaultListViewAdapter defaultListViewAdapter;

    @BindView(R.id.defaultLV)       ListView defaultLV;

    @State String name;

    public static SampleListViewFragment newInstance() {
        SampleListViewFragment fragment = new SampleListViewFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_listview;
    }

    @Override
    public void onViewReady() {
        setUpListView();
    }

    private void setUpListView(){
        defaultListViewAdapter = new DefaultListViewAdapter(getContext());
        defaultListViewAdapter.setNewData(getDefaultData());
        defaultLV.setAdapter(defaultListViewAdapter);
    }

    private List<SampleModel> getDefaultData(){
        List<SampleModel> androidModels = new ArrayList<>();
        SampleModel defaultItem;
        for(int i = 0; i < 20; i++){
            defaultItem = new SampleModel();
            defaultItem.id = i;
            defaultItem.name = "name " + i;
            androidModels.add(defaultItem);
        }
        return androidModels;
    }
}
