package com.appgradingtechnology.labyalo.android.fragment.startup;

import android.util.Log;

import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.activity.LandingActivity;
import com.appgradingtechnology.labyalo.server.request.Auth;
import com.appgradingtechnology.labyalo.vendor.android.base.BaseFragment;
import com.appgradingtechnology.labyalo.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class StartUpFragment extends BaseFragment {
    public static final String TAG = StartUpFragment.class.getName().toString();
    private LandingActivity activity;


    public static StartUpFragment newInstance() {
        StartUpFragment fragment = new StartUpFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_startup;
    }

    @Override
    public void onViewReady() {
   activity = (LandingActivity) getContext();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @OnClick(R.id.loginBTN)
    void loginBTN(){
       activity.startLandingActivity("login");
    }
    @OnClick(R.id.signUpBTN)
    void signUpBTN(){
        activity.startRegisterActivity("first_screen");
    }
}
