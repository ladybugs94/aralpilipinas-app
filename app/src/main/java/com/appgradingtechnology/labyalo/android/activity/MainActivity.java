package com.appgradingtechnology.labyalo.android.activity;

import android.media.Image;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.adapter.DrawerAdapter;
import com.appgradingtechnology.labyalo.android.fragment.DefaultFragment;
import com.appgradingtechnology.labyalo.android.fragment.main.ConnectionTypeFragment;
import com.appgradingtechnology.labyalo.android.fragment.main.MainFragment;
import com.appgradingtechnology.labyalo.android.fragment.profile.ConnectFragment;
import com.appgradingtechnology.labyalo.android.fragment.profile.ProfileFragment;
import com.appgradingtechnology.labyalo.android.route.RouteActivity;
import com.appgradingtechnology.labyalo.data.model.api.NavDrawerModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class MainActivity extends RouteActivity {
    public static final String TAG = MainActivity.class.getName().toString();
    private MainActivity activity;

//
//    DrawerLayout drawer_layout;
//    private DrawerAdapter drawerAdapter;
//    @BindView(R.id.drawerLV)
//    ListView drawerLV;
//
//    @BindView(R.id.drawer_layout)

    @Override
    public int onLayoutSet() {
        return R.layout.activity_main;
    }

    @Override
    public void onViewReady() {
        activity = (MainActivity) getContext();
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName) {
            case "main":
                openMainFragment();
                break;

            default:
                openDefaultFragment();
                break;
        }
    }

    public void openDefaultFragment() {
        switchFragment(DefaultFragment.newInstance());
    }

    public void openMainFragment() {
        switchFragment(MainFragment.newInstance());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

//    public void setSelectedItem(String item) {
//        drawerAdapter.setSelectedItem(item);
//    }
//
//    private void SetDrawer() {
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                activity, drawer_layout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer_layout.setDrawerListener(toggle);
//        toggle.syncState();
//
//        drawerAdapter = new DrawerAdapter(getContext());
////        drawerAdapter.setClickListener(this);
//        drawerAdapter.setNewData(NavDrawer());
//
//        drawerLV.setAdapter(drawerAdapter);
//    }


//    private List<NavDrawerModel> NavDrawer() {
//        List<NavDrawerModel> navDrawerListModels = new ArrayList<>();
//
//        NavDrawerModel navDrawerModel = new NavDrawerModel();
//        navDrawerModel.id = 1;
//        navDrawerModel.item = "Home";
//        navDrawerListModels.add(navDrawerModel);
//
//        navDrawerModel = new NavDrawerModel();
//        navDrawerModel.id = 2;
//        navDrawerModel.item = "Settings";
//        navDrawerListModels.add(navDrawerModel);
//
//        return navDrawerListModels;
//    }
//
//    public void drawer() {
//        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
//            drawer_layout.closeDrawer(GravityCompat.START);
//        } else {
//            drawer_layout.openDrawer(GravityCompat.START);
//        }
//    }
//
//
//    @OnClick(R.id.menuBTN)
//    void menuBTNOnClicked() {
//        drawer();
//    }

}
