package com.appgradingtechnology.labyalo.android.fragment.register;

import android.icu.util.Calendar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.activity.RegisterActivity;
import com.appgradingtechnology.labyalo.data.model.api.AccountModel;
import com.appgradingtechnology.labyalo.server.request.Auth;
import com.appgradingtechnology.labyalo.vendor.android.base.BaseFragment;
import com.appgradingtechnology.labyalo.vendor.server.transformer.BaseTransformer;
import com.tiper.MaterialSpinner;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SecondRegisterFragment extends BaseFragment implements View.OnClickListener, MaterialSpinner.OnItemSelectedListener {
    public static final String TAG = SecondRegisterFragment.class.getName().toString();
    private RegisterActivity activity ;
    @BindView(R.id.nxtBTN)
    TextView nxtBTN;
    @BindView(R.id.fnameET) EditText fnameET;
    @BindView(R.id.lnameET) EditText lnameET;
    @BindView(R.id.emailET) EditText emailET;
    @BindView(R.id.contactET) EditText contactET;
    @BindView(R.id.addressET) EditText addressET;
    @BindView(R.id.passwordET) EditText passwordET;
    @BindView(R.id.cPaswordET) EditText cPaswordET;
    @BindView(R.id.materialSpinnerMonth) MaterialSpinner materialSpinnerMonth;
    @BindView(R.id.materialSpinnerDay)   MaterialSpinner materialSpinnerDay;
    @BindView(R.id.materialSpinnerYear)  MaterialSpinner materialSpinnerYear;
    String month, date, year;


    public static SecondRegisterFragment newInstance() {
        SecondRegisterFragment fragment = new SecondRegisterFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_second_reg;
    }

    @Override
    public void onViewReady() {
        activity = (RegisterActivity) getContext();
        nxtBTN.setOnClickListener(this);
        materialSpinnerMonth.setOnItemSelectedListener(this);
        materialSpinnerDay.setOnItemSelectedListener(this);
        materialSpinnerYear.setOnItemSelectedListener(this);
        populateSpinnerMonth();
        populateSpinnerDay();
        populateSpinnerYear();
    }

    private void populateSpinnerYear() {
        List<String> years = new ArrayList<String>();
        int thisYear = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            thisYear = Calendar.getInstance().get(Calendar.YEAR);
        }
        for (int i = 1900; i <= thisYear; i++) {
            years.add(Integer.toString(i));
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, years);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        materialSpinnerYear.setAdapter(dataAdapter);
    }

    private void populateSpinnerDay() {
        List<String> days = new ArrayList<String>();
        for (int i = 1; i <= 31; i++) {
            days.add(Integer.toString(i));
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, days);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        materialSpinnerDay.setAdapter(dataAdapter);
    }

    private void populateSpinnerMonth() {
        List<String> months = new ArrayList<String>();
        for (int i = 1; i <= 12; i++) {
            months.add(Integer.toString(i));
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, months);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        materialSpinnerMonth.setAdapter(dataAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }
    @Override
    public void onClick(View view) {
          switch (view.getId()){
              case R.id.nxtBTN:
                 if(!fnameET.getText().toString().trim().isEmpty() &&  !lnameET.getText().toString().trim().isEmpty()  && !emailET.getText().toString().trim().isEmpty() && !contactET.getText().toString().trim().isEmpty()  && !passwordET.getText().toString().trim().isEmpty() && !cPaswordET.getText().toString().trim().isEmpty())  {
                   if(isPasswordMatch(passwordET.getText().toString().trim(), cPaswordET.getText().toString().trim())) {
                       String birth_date = year + "-" + month + "-" + date;
                       AccountModel accountModel = new AccountModel();
                       accountModel.firstname = fnameET.getText().toString().trim();
                       accountModel.lastname = lnameET.getText().toString().trim();
                       accountModel.birthdate = birth_date;
                       accountModel.email = emailET.getText().toString().trim();
                       accountModel.contactNumber = contactET.getText().toString().trim();
                       accountModel.password = passwordET.getText().toString().trim();
                       accountModel.password_confirmation = cPaswordET.getText().toString().trim();
                       activity.setAccountModel(accountModel);
                       activity.openThirdFragment();
                   }
                   else
                      Toast.makeText(activity, "Password MisMatch", Toast.LENGTH_SHORT).show();

                 }
                 else {
                     if(fnameET.getText().toString().trim().isEmpty()){
                         fnameET.setError("Field must not empty!");
                     }
                     else if(lnameET.getText().toString().trim().isEmpty()){
                         lnameET.setError("Field must not empty!");
                     }
                     else if(emailET.getText().toString().trim().isEmpty()){
                         lnameET.setError("Field must not empty!");
                     }
                     else if(contactET.getText().toString().trim().isEmpty()){
                         lnameET.setError("Field must not empty!");
                     }
                     else if(passwordET.getText().toString().trim().isEmpty()){
                         lnameET.setError("Field must not empty!");
                     }
                     else if(cPaswordET.getText().toString().trim().isEmpty()){
                         lnameET.setError("Field must not empty!");
                     }


                 }
                  break;
          }
    }

    @Override
    public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view, int i, long l) {
              switch (materialSpinner.getId()){
                  case  R.id.materialSpinnerMonth:
                      month = materialSpinnerMonth.getSelectedItem().toString();
                      break;
                  case R.id.materialSpinnerDay:
                      date = materialSpinnerDay.getSelectedItem().toString();
                      break;
                  case R.id.materialSpinnerYear:
                      year = materialSpinnerYear.getSelectedItem().toString();
                      break;
                  default:

                      break;

              }
    }

    @Override
    public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {

    }


    public  boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean isPasswordMatch(String pass, String cpass){
           if(pass.equals(cpass)){
               return true;
           }
           return false;
    }
}
