package com.appgradingtechnology.labyalo.android.activity;

import com.appgradingtechnology.labyalo.R;
import com.appgradingtechnology.labyalo.android.fragment.DefaultFragment;
import com.appgradingtechnology.labyalo.android.fragment.main.ConnectionTypeFragment;
import com.appgradingtechnology.labyalo.android.fragment.profile.ConnectFragment;
import com.appgradingtechnology.labyalo.android.fragment.profile.ProfileFragment;
import com.appgradingtechnology.labyalo.android.fragment.register.ThirdRegisterFragment;
import com.appgradingtechnology.labyalo.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class AnotherActivity extends RouteActivity {
    public static final String TAG = AnotherActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_another;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "profile":
                openProfileFragment();
                break;
            case "connect":
                openConnectFragment();
                break;
            case "plan":
                openPlanFragment();
                break;

        }
    }

    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }
    public void openProfileFragment() {
        switchFragment(ProfileFragment.newInstance());
    }
    public void openConnectFragment() {
        switchFragment(ConnectFragment.newInstance());
    }
    public void openPlanFragment() {
        switchFragment(ConnectionTypeFragment.newInstance());
    }

}
