package com.appgradingtechnology.labyalo.data.model.api;

import com.appgradingtechnology.labyalo.vendor.android.base.AndroidModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class AccountModel extends AndroidModel {

    @SerializedName("firstname")
    public String firstname;
    @SerializedName("lastname")
    public String lastname;
    @SerializedName("birthdate")
    public String birthdate;
    @SerializedName("email")
    public String email;
    @SerializedName("contact_number")
    public String contactNumber;
    @SerializedName("address")
    public String address;
    @SerializedName("password")
    public String password;
    @SerializedName("password_confirmation")
    public String password_confirmation;
    @SerializedName("school_name")
    public String school_name;
    @SerializedName("student_id")
    public String student_id;
    @SerializedName("school_level")
    public String school_level;
    @SerializedName("school_validation_code")
    public String school_validation_code;


    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public AccountModel convertFromJson(String json) {
        return convertFromJson(json, AccountModel.class);
    }
}
