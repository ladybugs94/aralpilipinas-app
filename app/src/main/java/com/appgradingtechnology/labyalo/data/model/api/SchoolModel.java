package com.appgradingtechnology.labyalo.data.model.api;

import com.appgradingtechnology.labyalo.vendor.android.base.AndroidModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SchoolModel extends AndroidModel {


    @SerializedName("name")
    public String name;
    @SerializedName("address")
    public String address;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public SchoolModel convertFromJson(String json) {
        return convertFromJson(json, SchoolModel.class);
    }
}
