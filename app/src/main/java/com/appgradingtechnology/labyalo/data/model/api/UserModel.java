package com.appgradingtechnology.labyalo.data.model.api;

import com.appgradingtechnology.labyalo.vendor.android.base.AndroidModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class UserModel extends AndroidModel {


    @SerializedName("firstname")
    public String firstname;
    @SerializedName("lastname")
    public String lastname;
    @SerializedName("birthdate")
    public String birthdate;
    @SerializedName("email")
    public String email;
    @SerializedName("contact_number")
    public String contactNumber;
    @SerializedName("address")
    public String address;
    @SerializedName("password")
    public String password;
    @SerializedName("password_confirmation")
    public String password_confirmation;

    @SerializedName("school")
    public String school;
    @SerializedName("student_id")
    public String student_id;
    @SerializedName("school_level")
    public String school_level;
    @SerializedName("school_validation_code")
    public String school_validation_code;
    @SerializedName("pin_password")
    public String pin_password;
    @SerializedName("date_created")
    public DateCreatedBean dateCreated;
    @SerializedName("avatar")
    public AvatarBean avatar;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public UserModel convertFromJson(String json) {
        return convertFromJson(json, UserModel.class);
    }

    public static class DateCreatedBean {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class AvatarBean {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }
}
